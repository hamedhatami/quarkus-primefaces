package com.web.service;


import org.primefaces.model.SortOrder;

import java.util.Comparator;

public class LazySorter implements Comparator<Car> {

    private final String sortField;
    private final SortOrder sortOrder;

    public LazySorter(String sortField, SortOrder sortOrder) {
        this.sortField = sortField;
        this.sortOrder = sortOrder;
    }

    @Override
    public int compare(Car car1, Car car2) {
        try {
            Object value1 = Car.class.getMethod("get" + capitalize(this.sortField)).invoke(car1);
            Object value2 = Car.class.getMethod("get" + capitalize(this.sortField)).invoke(car2);

            int value = ((Comparable) value1).compareTo(value2);

            return SortOrder.ASCENDING.equals(sortOrder) ? value : -1 * value;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public String capitalize(String value) {
        if (value == null || value.trim().isEmpty()) {
            return null;
        }

        return value.substring(0, 1).toUpperCase() + value.substring(1);
    }
}
